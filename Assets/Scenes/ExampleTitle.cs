using EltaninEntertainment.Commons.DataManagement;
using EltaninEntertainment.Commons.Menus;
using EltaninEntertainment.RPG.Party;
using EltaninEntertainment.Commons.SceneControllers;
using UnityEngine;

public class ExampleTitle : MonoBehaviour
{
    [SerializeField] private SceneReference scene;

    private void Start()
    {
        PartyManager pm = GameManager.Instance?.GetData<PartyManager>() as PartyManager;

        scene = SceneLoader.GetSceneFromPath(pm.storedData.map);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space))
        {
            SceneLoader.LoadScene(scene);

            PauseMenu.Instance?.ToggleMenuButton(true);
        }
    }
}
