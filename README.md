# Overview
This is a generic base, for Turn-Based RPGs to be built upon, in Unity.

# Features
Right now, this template features the following:

- A Turn-Based Battle System, still in development but fully-functional at a basic level.
- Saving and Loading - Formats data in JSON format.
- A menu, complete with a party screen and inventory screen, which also allows you to use and toss the items (use doesn't function just yet).
- A generic movement class which has its functionality implemented by a PlayerController and an NPCController.
- Interaction with NPC's Enemies and Item Chests in the overworld.
- Dialogue